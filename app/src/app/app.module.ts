import { SearchPipe } from './shared/pipes/search.pipe';
import { ErrorsComponent } from './shared/components/errors/errors.component';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemsComponent } from './features/items/items.component';
import { WorkersComponent } from './features/workers/workers.component';
import { RegisterComponent } from './features/register/register.component';
import { ProfilComponent } from './features/profil/profil.component';
import { SearchComponent } from './shared/components/search/search.component';
import { GridComponent } from './shared/components/grid/grid.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ItemComponent } from './features/item/item.component';
import { AuthComponent } from './shared/components/auth/auth.component';
import { AuthInterceptor } from './shared/utils/auth.interceptor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AddItemComponent } from './components/add-item/add-item.component';

@NgModule({
  declarations: [
    AppComponent,
    ErrorsComponent,
    SearchPipe,
    ItemsComponent,
    WorkersComponent,
    RegisterComponent,
    ProfilComponent,
    SearchComponent,
    GridComponent,
    ItemComponent,
    AuthComponent,
    AddItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule
  ],
  providers: [
    {provide:HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
