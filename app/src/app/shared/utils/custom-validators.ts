import { AbstractControl, FormGroup, ValidationErrors } from '@angular/forms';

export class CustomValidators {
  static samePassword(passw: AbstractControl) : ValidationErrors|null{
    if(passw.value.password === passw.value.confirmPassword) return null
    passw.value.password
    return {samePassword: true}
  }
  static atLeastOneShoudBeSelected(
    group: AbstractControl
  ): ValidationErrors | null {
    // for (const key in group.value) {
    //   if (group.value[key]) {
    //     return null;
    //   }
    // }
    if (Object.values(group.value).includes(true)) return null;

    return { atLeastOneShoudBeSelected: true };
  }

  static shoutBePassed(control: AbstractControl): ValidationErrors | null {
    if (Date.parse(control.value) < Date.now()) {
      return null;
    }
    return { shoutBePassed: true };
  }
}
