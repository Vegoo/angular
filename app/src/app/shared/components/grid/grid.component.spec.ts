import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { GridComponent } from './grid.component';

fdescribe('GridComponent', () => {
  let component: GridComponent;
  let fixture: ComponentFixture<GridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display 2 rows', ()=>{
    component.data = [{title:'tomato'}, {title:'potatoes'}];
    component.config = [{key:'title'}];
    fixture.detectChanges();
    const tpl = fixture.debugElement;
    const rows = tpl.queryAll(By.css('tbody tr'));
    console.log('***',rows.length);
    expect(rows.length).toBe(2)
  })


  it('should display 3 col in first row',()=>{
    component.data = [{title:'tomato'}, {title:'potatoes'}];
    component.config = [{key:'title'},{key:'price'},{key:'phone'}];
    fixture.detectChanges();
    const tpl = fixture.debugElement;
    console.log(tpl);
    const rows = tpl.query(By.css('tbody tr'));
    const cols = rows.queryAll(By.css('td'))
    console.log('***',cols.length);
    expect(cols.length).toBe(4)
  })

});
