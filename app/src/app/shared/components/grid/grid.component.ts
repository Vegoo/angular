import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { GridDataModel, GridFieldTypes } from '../../utils/models';



@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {

  @Input() data!:any[];
  @Input() config!:GridDataModel<any>[];
  @Output() action = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onClick(id:string, type:string|undefined) {
    this.action.emit({id, type})
  }

}
