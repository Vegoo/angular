import { Component, OnInit, Input, ViewChild, AfterViewInit, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { debounce, debounceTime, filter } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, AfterViewInit {

  @ViewChild('f') form!:NgForm;
  @Input() controls!:any;
  @Output() action = new EventEmitter();
  constructor() { }


  ngAfterViewInit(): void {
    this.form.valueChanges?.pipe(
      filter((val: any) => {
        if (JSON.stringify(Object.values(val)).includes('dupa')) {
          alert('sam jesteś dupa');
          return false
        } else {
          return true;
        }
      }),
      debounceTime(1111)
    ).subscribe((value)=>{
      this.action.emit(value)
    })
  }

  ngOnInit(): void {
  }

}
