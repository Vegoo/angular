import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { WorkersService } from 'src/app/services/workers.service';
import { GridDataModel, WorkerModel, WorkersKeys } from 'src/app/shared/utils/models';

@Component({
  selector: 'app-workers',
  templateUrl: './workers.component.html',
  styleUrls: ['./workers.component.scss']
})
export class WorkersComponent implements OnInit {


  filters: any;
  // wData!: any[];
  workers$:Observable<WorkerModel[] | null> = this.workersService.fetch()
  .pipe(map((resp) => {
    return resp.data
  }))
  wConfig: GridDataModel<WorkersKeys>[] = [
    {key:'name'},
    {key:'category'},
    {key:'phone'},
    {type:'button', text:'more'},
    {type:'button', text:'delete'}
  ]
  constructor(
    private workersService: WorkersService
  ) {}

  ngOnInit(): void {

  }
    // this.workersService.fetch().subscribe((resp)=>{
    //   this.wData = resp.data

}
