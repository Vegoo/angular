import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { ItemsService } from 'src/app/services/items.service';
import { GridDataModel, HttpResponseModel, ItemsFiltersModel, ItemsKeys } from 'src/app/shared/utils/models';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {

  gdata!: any[];
  total!: number;
  gConfig: GridDataModel<ItemsKeys>[] = [
    {key: 'title'},
    {key: 'price', type: 'input'},
    {key: 'imgSrc', type: 'image'},
    {type: 'button', text:'more'},
    {type: 'button', text:'delete'}
  ]
  filters: BehaviorSubject<ItemsFiltersModel> = new BehaviorSubject<ItemsFiltersModel>({
    itemsPerPage: 5,
    currentPage: 1
  })
  constructor(
    private itemService: ItemsService,
    private router: Router
  ) {
    this.filters.subscribe((val) => {
      this.fetchItems();
    })
  }

  updataFilters(obj: {}) {
    this.filters.next({ ...this.filters.value, ...obj })
  }

  xyz(value:any){
    this.filters.next({...this.filters.value, ...value})
  }

  onAction(action:{id:string, type:string|undefined}){
    switch (action.type) {
      case 'more':
        this.router.navigate(['items', action.id])
        break;
      case 'delete':
        this.itemService.remove(action.id).subscribe(()=>{
          this.fetchItems();
        })
        break;
    }
  }

  ngOnInit(): void {
    this.fetchItems();
  }


  public fetchItems() {
    this.itemService.fetch(this.filters.value).subscribe((resp: HttpResponseModel) => {
      this.gdata = resp.data;
      this.total = resp.total;
    });
  }




}
