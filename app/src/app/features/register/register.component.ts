import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Api } from 'src/app/shared/utils/api';
import { CustomValidators } from 'src/app/shared/utils/custom-validators';
import { HttpServiceModel } from 'src/app/shared/utils/models';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {


  form = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.email
    ], this.checkUsername.bind(this)),
    birthDate: new FormControl(null,[
      Validators.required,
      CustomValidators.shoutBePassed
    ]),
    hobbies: new FormGroup({
      disco: new FormControl(),
      rock: new FormControl(),
      metal: new FormControl()
    },[
      CustomValidators.atLeastOneShoudBeSelected
    ]),
    passwordsGroup: new FormGroup({
      password: new FormControl(),
      confirmPassword: new FormControl(),
    },[
      CustomValidators.samePassword
    ]),
    questions: new FormArray([
      new FormControl('asd')
    ]),
  })
  constructor(
    private http:HttpClient
  ) { }

  ngOnInit(): void {
  }

  order(idx:any,item:any){
    return idx//item.key
  }
  addQ(){
    (this.form.controls.questions as FormArray).push(new FormControl(''))
  }
  checkUsername(control: AbstractControl): Observable<any> {
    return this.http
      .get(Api.AUTH_CHECK_USERNAME + "?username=" + control.value)
      .pipe(
        map((value: any) => {
          if (value.error) {
            return { servererror: true }
          } else {
            return null;
          }
        })
      )
  }
}
