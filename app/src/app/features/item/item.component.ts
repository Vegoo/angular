import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ItemsService } from 'src/app/services/items.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  data: any;

  data$:Observable<any> = this.route.data.pipe(map((res)=>res.item));
  constructor(
    // private itemsService:ItemsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    // console.log(this.route.snapshot.params.id);
    // this.itemsService.get(this.route.snapshot.params.id).subscribe((resp:any)=>{
    //   this.data = resp.data;
    }
  }
