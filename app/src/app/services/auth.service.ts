import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Api } from '../shared/utils/api';
import { AuthDataModel, AuthServiceInterface, HttpResponseModel } from '../shared/utils/models';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements AuthServiceInterface {
  private _access: boolean = false;

  public get access(): boolean {
    return this._access;
  }

  constructor(
    private http:HttpClient
  ) {
    this.logged();
   }

  logged(): void {
    this.http.get<HttpResponseModel>(Api.AUTH_IS_LOGGED).subscribe((resp: HttpResponseModel) => {
      !resp.error && (this._access = true);
    })
  }
  logIn(value: AuthDataModel): void {
    this.http.post<HttpResponseModel>(Api.AUTH_LOGIN, value).subscribe((resp:HttpResponseModel)=>{
      this._access = true
      localStorage.setItem('token', resp.accessToken)
    })
  }
  logOut(): void {
    this._access = false;
    localStorage.removeItem('token');
  }
}
