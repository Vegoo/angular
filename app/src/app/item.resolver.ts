import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ItemsService } from './services/items.service';

@Injectable({
  providedIn: 'root'
})
export class ItemResolver implements Resolve<any> {

  constructor(
    private itemsService:ItemsService
  ){}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const id = route.params.id
    return this.itemsService.get(id).pipe(map((resp:any)=>resp.data))
  }
}
