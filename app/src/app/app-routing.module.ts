import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'dist/app/auth.guard';
import { ItemComponent } from './features/item/item.component';
import { ItemsComponent } from './features/items/items.component';
import { ProfilComponent } from './features/profil/profil.component';
import { RegisterComponent } from './features/register/register.component';
import { WorkersComponent } from './features/workers/workers.component';
import { ItemResolver } from './item.resolver';

const routes: Routes = [
  {path: 'items', component: ItemsComponent},
  {path: 'items/:id', resolve:{item: ItemResolver}, component: ItemComponent},
  {path: 'workers', component: WorkersComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'profile', canActivate:[AuthGuard], component: ProfilComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


