import { Component, OnInit, TemplateRef, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ItemsService } from 'src/app/services/items.service';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {


  @Output() close = new EventEmitter();

  constructor(
    private modalService: NgbModal,
    private itemsService: ItemsService
  ) { }

  ngOnInit(): void {
  }

  open(tpl: TemplateRef<any>){
    this.modalService.open(tpl)
  }

  send(value: NgForm){
    if(value.valid){
     this.itemsService.add(value.value).subscribe((resp)=>{
      this.close.emit()
      this.modalService.dismissAll();
     })
    } else {
      console.warn('form invalid')
    }
  }
}
