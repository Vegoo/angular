describe('My First Test', () => {

  const uniqueTitle = 'Arek' + Date.now();

  it('Visits the initial project page', () => {
    cy.visit('/items')
    cy.contains('items')

    cy.get('app-auth button').contains('send').click();

    cy.contains('new').click();


    cy.get('#add-items-form input[name=title]').type(uniqueTitle)
    cy.get('#add-items-form input[name=price]').type('123')
    cy.get('#add-items-form select').select('food')
    cy.get('#add-items-form button').click();


    cy.get('app-search input').first().type(uniqueTitle)

    cy.get('app-grid tbody >tr').should('have.length', 1)
    cy.contains('app-grid tbody >tr').contains(uniqueTitle)

    cy.get('app-grid tbody >tr').contains(uniqueTitle)


  })
})
